Установить все зависимости
`yarn install`

Собрать проект
`gulp build`

Следить за изменениями
`gulp`

Собрать продакшн
`NODE_ENV=production gulp build`

Очистить проект
`gulp clean`