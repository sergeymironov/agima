'use strict';

const fs = require('fs');
const del = require('del');
const path = require('path');
const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssnano = require('gulp-cssnano');
const autoprefixer = require('autoprefixer');
const sortCSSmq = require('sort-css-media-queries');
const mqpacker = require('css-mqpacker');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const gulpif = require('gulp-if');

const changed = require('gulp-changed');
const nunjucksRender = require('gulp-nunjucks-render');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

const browserSync = require('browser-sync').create();
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

sass.compiler = require('node-sass');

const srcPath = './src/';
const buildPath = './build/';

const paths = {
    src: {
        html: srcPath,
        template: srcPath + 'template/',
        style: srcPath + 'style/',
        fonts: srcPath + 'fonts/',
        libs: srcPath + 'libs/',
        img: srcPath + 'img/',
    },
    build: {
        html: buildPath,
        css: buildPath + 'css/',
        fonts: buildPath + 'fonts/',
        libs: buildPath + 'libs/',
        img: buildPath + 'img/',
    }
};

gulp.task('html', () => {
    return gulp.src(paths.src.html + '*.html')
        .pipe(changed(paths.build.html, {
            extension: '.html'
        }))
        .pipe(nunjucksRender({
            path: [paths.src.template]
        }))
        .pipe(gulp.dest(paths.build.html));
});

gulp.task('template', () => {
    return gulp.src(paths.src.html + '*.html')
        .pipe(nunjucksRender({
            path: [paths.src.template]
        }))
        .pipe(gulp.dest(paths.build.html));
});

gulp.task('style', () => {
    return gulp.src(paths.src.style + '**/*.scss')
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: err.plugin,
                message: err.message
            }))
        }))
        .pipe(sass({
            outputStyle: 'expanded',
            precision: 8
        }))
        .pipe(postcss([
            autoprefixer({
                browsers: ['last 2 versions']
            }),
            mqpacker({
                sort: sortCSSmq
            })
        ]))
        .pipe(gulpif(!isDevelopment, cssnano()))
        .pipe(gulp.dest(paths.build.css));
});

gulp.task('fonts', () => {
    return gulp.src(paths.src.fonts + '**/*.{ttf,eot,svg,woff,woff2}')
        .pipe(changed(paths.build.fonts))
        .pipe(gulp.dest(paths.build.fonts));
});

gulp.task('libs', () => {
    return gulp.src(paths.src.libs + '**/*.*')
        .pipe(changed(paths.build.libs))
        .pipe(gulp.dest(paths.build.libs));
});

gulp.task('img', () => {
    return gulp.src(paths.src.img + '**/*.{jpg,png,gif,svg}')
        .pipe(changed(paths.build.img))
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.build.img));
});

gulp.task('clean', () => {
    return del([buildPath], {
        force: true
    });
});

gulp.task('serve', () => {
    browserSync.init({
        server: buildPath
    });

    browserSync.watch(buildPath + '**/*.*').on('change', browserSync.reload);
});

gulp.task('watch', () => {
    gulp.watch(paths.src.html + '*.html', gulp.series('html'));
    gulp.watch(paths.src.template + '*.html', gulp.series('template'));
    gulp.watch(paths.src.style + '**/*.scss', gulp.series('style'));
    gulp.watch(paths.src.fonts + '**/*.{ttf,eot,svg,woff,woff2}', gulp.series('fonts')).on('unlink', filepath => {
        del.sync(path.resolve(paths.build.fonts, path.relative(path.resolve(paths.src.fonts), filepath)), {
            force: true
        });
    });
    gulp.watch(paths.src.libs + '**/*.*', gulp.series('libs')).on('unlink', filepath => {
        del.sync(path.resolve(paths.build.libs, path.relative(path.resolve(paths.src.libs), filepath)), {
            force: true
        });
    });
    gulp.watch(paths.src.img + '**/*.{jpg,png,gif,svg}', gulp.series('img')).on('unlink', filepath => {
        del.sync(path.resolve(paths.build.img, path.relative(path.resolve(paths.src.img), filepath)), {
            force: true
        });
    });
});

gulp.task('build', gulp.series('clean', gulp.parallel('html', 'style', 'fonts', 'libs', 'img')));
gulp.task('default', gulp.series('build', gulp.parallel('watch', 'serve')));